var app = new Vue({
    el: '#app',
    created: function() {
        const div = document.getElementById('div').textContent;
        const words = div.split(' ');
        const obj = {};
        words.forEach(el => {
            obj[el] = (obj[el] + 1) || 1;
            const result = document.createElement('div');
            result.innerHTML = `${el} ${obj[el]}`;
            document.body.appendChild(result);
        });
    }
})